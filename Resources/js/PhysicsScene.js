var PhysicsScene = function(){};

var controller;
var winSize;
var space;

PhysicsScene.prototype.onDidLoadFromCCB = function()
{
	winSize = cc.Director.getInstance().getWinSize();
	controller = this.rootNode;
	
	this.init();
	
	if (sys.os == "android" || sys.os == "Android") {
        this.rootNode.setKeypadEnabled(true);
	}

	this.rootNode.backClicked = function() {
		this.controller.backClicked();
		return true;
	}

	this.rootNode.onTouchesBegan = function(touches, event) {	
		this.controller.onTouchesBegan(touches, event);
		return true;
       }	
        
	this.rootNode.onTouchesMoved = function(touches, event) {	
		this.controller.onTouchesMoved(touches, event);
		return true;
    }	

    this.rootNode.onTouchesEnded = function(touches, event) {	
		this.controller.onTouchesEnded(touches, event);
		return true;
    }

	this.rootNode.onEnterTransitionDidFinish = function() {
		setStopAction(null, false);
		return true;
	}

	this.rootNode.schedule(update);
}

PhysicsScene.prototype.backClicked = function () {
	var scene = cc.BuilderReader.load("MainScene");
    cc.Director.getInstance().replaceScene(scene);
};

PhysicsScene.prototype.onTouchesBegan = function(touches, event) 
{
	var touch = touches[0];

	return true;
}

PhysicsScene.prototype.onTouchesMoved = function(touches, event) {
	var touch = touches[0];
	
	return true;
}

PhysicsScene.prototype.onTouchesEnded = function(touches, event) {
	var touch = touches[0];
	
	return true;
}

var bodyDefs;

PhysicsScene.prototype.init = function() {
	cc.Director.getInstance().setDisplayStats(false);
	initPhysics();
	initDebugMode(controller);

	addWallsAndGround();
	addPhysicsCircle();
	addPhysicsBox("square.png");
	addPhysicsBox("rectangle.png");

	addStaticObject();
	//bodyDefs = [];
	//shapeCache = gcp.ShapeCache.getInstance();
	//shapeCache.addShapesWithFile("lion.plist");
	//addPhysicsPoly("lion.png");
}

function initPhysics() {
	space = new cp.Space();
	space.gravity = cp.v(0, -800);
	space.iterations = 30;
	space.sleepTimeThreshold = Infinity;
	space.collisionSlop = Infinity;
}

function initDebugMode(controller) {
	var phDebugNode = cc.PhysicsDebugNode.create(space);
	controller.addChild(phDebugNode, 10);
}

var updateStepValue = 60;
function update() {
	space.step(1/updateStepValue);
}

var WALLS_WIDTH = 10;
var WALLS_ELASTICITY = 1;
var WALLS_FRICTION = 1;

function addWallsAndGround() {
	leftWall = new cp.SegmentShape(space.staticBody, new cp.v(0, 0), new cp.v(0, winSize.height), WALLS_WIDTH);
	leftWall.setElasticity(WALLS_ELASTICITY);
	leftWall.setFriction(WALLS_FRICTION);
	space.addStaticShape(leftWall);
	
	rightWall = new cp.SegmentShape(space.staticBody, new cp.v(winSize.width, winSize.height), new cp.v(winSize.width, 0), WALLS_WIDTH);
	rightWall.setElasticity(WALLS_ELASTICITY);
	rightWall.setFriction(WALLS_FRICTION);
	space.addStaticShape(rightWall);
	
	bottomWall = new cp.SegmentShape(space.staticBody, new cp.v(0, 0), new cp.v(winSize.width, 0), WALLS_WIDTH);
	bottomWall.setElasticity(WALLS_ELASTICITY);
	bottomWall.setFriction(WALLS_FRICTION);
	space.addStaticShape(bottomWall);

	topWall = new cp.SegmentShape(space.staticBody, new cp.v(0, winSize.height), new cp.v(winSize.width, winSize.height), WALLS_WIDTH);
	topWall.setElasticity(WALLS_ELASTICITY);
	topWall.setFriction(WALLS_FRICTION);
	space.addStaticShape(topWall);
}

function addPhysicsCircle() {
	circle = cc.Sprite.create("circle.png");
	mass = 10;
	
	var nodeSize = circle.getContentSize(),
		phNode = cc.PhysicsSprite.create("circle.png"),
		phBody = null,
		phShape = null,
		scaleX = 1,
		scaleY = 1;
	nodeSize.width *= scaleX;
	nodeSize.height *= scaleY;
			
	phBody = space.addBody(new cp.Body(mass, cp.momentForBox(mass, nodeSize.width, nodeSize.height)));
	phBody.setPos(cc.p(winSize.width * Math.random(), winSize.height * Math.random()));
	
	phShape = space.addShape(new cp.CircleShape(phBody, nodeSize.width * 0.5, cc.p(0, 0)));
	phShape.setFriction(0.5);
	phShape.setElasticity(0.5);
	
	phNode.setBody(phBody);
	phNode.setRotation(0);
	phNode.setScale(1);
	
	controller.addChild(phNode);
}

function addPhysicsBox(filename) {
	box = cc.Sprite.create(filename);
	mass = 10;
	
	var nodeSize = box.getContentSize(),
		phNode = cc.PhysicsSprite.create(filename),
		phBody = null,
		phShape = null,
		scaleX = 1,
		scaleY = 1;
	nodeSize.width *= scaleX;
	nodeSize.height *= scaleY;
				
	phBody = space.addBody(new cp.Body(mass, cp.momentForBox(mass, nodeSize.width, nodeSize.height)));
	phBody.setPos(cc.p(winSize.width * Math.random(), winSize.height * Math.random()));
	
	shape = space.addShape(new cp.BoxShape(phBody, nodeSize.width, nodeSize.height));
	shape.setFriction(0.5);
	shape.setElasticity(0.5);
	
	phNode.setBody(phBody);
	phNode.setRotation(0);
	phNode.setScale(1);
	
	controller.addChild(phNode);
}


function addPhysicsPoly(filename) {
	cc.log("Physics poly for: "+filename);
	sprite = cc.Sprite.create(filename);
	controller.addChild(sprite);
	sprite.setPosition(cc.p(winSize.width * Math.random(), winSize.height * Math.random()));
	phObj = addPolyObject(controller, sprite, filename, bodyDefs[filename]);
			
}

function addPolyObject(controller, obj, spriteFileName, bd) {
	var nodeSize = obj.getContentSize(),
		phNode = cc.PhysicsSprite.create(spriteFileName),
		phBody = null,
		phShape = null,
		scale = 1;
	nodeSize.width *= scaleX;
	nodeSize.height *= scaleY;

	phBody = space.addBody(new cp.Body(mass, bd.momentum));
	phBody.p = bd.anchorPoint;
	phNode.setAnchorPoint(bd.anchorPoint);
	
	vzero = cp.vzero;
	bdPolygons = bd.fixtures[0].polygons;

	shapes = [];
	for (j = 0; j < bdPolygons.length; j++) {
		polyShape = new cp.PolyShape(phBody, bdPolygons[j].vertices, vzero);
	
		shape = space.addShape(polyShape);
	
		shape.setFriction(0.5);
		shape.setElasticity(0.5);
		shapes.push(shape);
	}

	phNode.setBody(phBody);
	phNode.setPosition(obj.getPosition());
	
	controller.addChild(phNode);
	//obj.setVisible(false);
}


function addStaticObject() {
	staticBody = new cp.Body(Infinity, Infinity);

	staticSprite = cc.Sprite.create("rectangle.png");
	controller.addChild(staticSprite);
	staticSprite.setPosition(cc.p(winSize.width * 0.5, winSize.height * 0.5));

	staticBody.setPos(staticSprite.getPosition());
	shape = new cp.BoxShape(staticBody, staticSprite.getContentSize().width, staticSprite.getContentSize().height); 
	shape.setElasticity(1);
	shape.setFriction(1);
	space.addShape(shape);
}










